# Flask App and  Helm chart for deployment on AWS EKS.

## Purpose

This repository consists of a simple python Flask and helm chart templates to host the python application on AWS EKS.



## Description
### Python Flask App
A simple python application using Flask Web Framework which renders an index file is present in ```app``` directory. This app runs on port 5000 and is containerized using Docker. 

### Helm Charts
The folder ``` k8s-app\eks-app ``` contains the helm charts that can be installed on AWS EKS . Its a simple helm  chart that mainly consists of a deployment, a serivce and an ingress .

## Build and Deployment Details
* The ```app``` folder consists of the python Flask app and index file. Using docker this will be containerized in the gitlab runner and pushed to ECR repo. 
Details on how ECR is built is available in the repo ``` https://gitlab.com/haneef20/k8scomponents.git ```

* The deployment into EKS is done following gitops principles and for that argocd is used. Argocd is installed following the repo ```https://gitlab.com/haneef20/k8scomponents.git ```. Inorder to update the manifest values file with the updated image tag the pipeline contains a job following the build .  

* ```k8s-app\eks-app\argocd``` folder contains the templates to create the argocd application. This is in combination with the repo ```https://gitlab.com/haneef20/k8scomponents.git ``` which creates a parent application and using it we will automate the creation of actual application that monitors the repo containing helm chart templates.
Details ```https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/``` 

### Enhancements and Security aspects that can be considered
* The Flask app is not production grade. We could consider using wsgi server .
* We could enable autoscaling of apps based on metrics using hpa which can be enabled by enabling autoscaling on helm chart values file.
* Should include docker image scans and tests in the ci/cd pipeline.
* We could consider using Mutlistage Builds in docker so that the final image includes  only what is required to required to run the application and is more secure.
* We should use https , this can be done in the ingress configuration by adding required annotations and we could use the AWS ACM service for the SSL Certs.

### Troubleshooting.
#### Once the app is deployed and if it cant be accessed using the browser please follow the below.
* Make sure the AWS Application Load Balancer is implemented and the dns points to it. The target group should be healthy . If the Load Balancer is not implemented please check for kubernetes events for any issues with ingress resource also verify AWS ALB ingress controller pod logs .
* Make sure the pods are running and the service has the right endpoints.
